#!/usr/bin/env make
#
# Makefile
#
# Copyright (C) 2020-2021 Franco Masotti (franco \D\o\T masotti {-A-T-} tutanota \D\o\T com)
#
# This file is part of fpydocs.
#
# fpydocs is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fpydocs is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with fpydocs.  If not, see <http://www.gnu.org/licenses/>.
#

export PACKAGE_NAME=fpydocs

default: install-dev

doc: clean
	pipenv run $(MAKE) -C docs html

install:
	@echo "setup not available for this project"

uninstall:
	@echo "setup not available for this project"

install-dev:
	pipenv install --dev
	pipenv run pre-commit install
	pipenv graph

uninstall-dev:
	rm -f Pipfile.lock
	pipenv --rm

update:
	pipenv run pre-commit autoupdate

test:
	@echo "tests not available for this project"

clean:
	rm -rf build dist *.egg-info
	pipenv run $(MAKE) -C docs clean

.PHONY: default doc install uninstall install-dev uninstall-dev test clean
