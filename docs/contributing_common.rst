Contributing [common]
=====================

Git branches
------------

What follows is a table of the git branches used in the repositories following fpydocs' specifications.

.. important:: Open pull requests on the ``dev`` target branch.
               Use ``bugfix-${fix_name}`` or ``newfeature-${new_feature_name}`` as names.

=====================================   ====================================================   ==============================
Branch                                  Description                                            Update schedule
=====================================   ====================================================   ==============================
``master``                              the main branch                                        every new release
``dev``                                 recent changes are merged here before a new release    at will
``bugfix-${fix_name}``                  a generic bug fix                                      -
``newfeature-${new_feature_name}``      a generic new feature                                  -
=====================================   ====================================================   ==============================

Development environment
-----------------------

1. install `Pipenv <https://pipenv.readthedocs.io/en/latest/>`_
2. run the following to install the development environment

   ::


       make install-dev


3. you can use ``pipenv run`` to run the program in
   the development environment
4. if you have changed parts of the source code you must take care of adding
   the corresponding unit tests if applicable.
5. run the tests with:


   ::


       make test


6. to install the program for the running user run:


   ::


       make install


   .. note:: don't forget to add ``~/.local/bin`` to ``PATH``.

7. to rebuild the documentation run


   ::


       make doc


TODO and FIXME
--------------

Go in the repository's root and then:


::

    $ grep -e TODO -e FIXME -n */*.py


Contribution Steps
------------------

1. clone the repository
2. install the requirements
3. write code
4. write unit tests
5. run tests
6. update relevant documentation, if necessary
7. pull request
